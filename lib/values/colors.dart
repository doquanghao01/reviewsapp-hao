import 'package:flutter/material.dart';

class AppResources {
  static const Color mainColors = Color.fromRGBO(264, 47, 180,1);

  static const Color textColorsTitle = Colors.white;
  static const Color textColorsTitle1 = Color.fromRGBO(36, 54, 84,1);

  static const Color textColorsSubTitle = Colors.white70;
  static const Color textColorsSubTitle1 = Colors.black54;

  static const Color textColorsContent = Colors.black;
  static const Color textColorsContent1 = Colors.black38;

  static const Color textColorsHint = Colors.black12;

  static const Color mainIcons = Colors.black26;
  static const Color mainIcons1 = Color.fromARGB(255, 47, 0, 255);

  static const Color containerBox = Colors.white;
  static const Color starColor = Color.fromRGBO(255, 165, 81,1);
  static const Color cardColor = Color.fromARGB(255, 224, 224, 224);

  static const Color boxCommentColor = Color.fromARGB(255, 240, 240, 240);
  static const Color rimColor = Colors.black54;
    static const Color buttomColor = Colors.black45;
    static const Color WhiteSmoke =  Color.fromRGBO(245, 245, 245,1);
}
