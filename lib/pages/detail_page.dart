import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewapp_hao/values/colors.dart';

import '../drawable/app_circle_avatar.dart';
import '../drawable/app_icons.dart';
import '../drawable/text_subtitle.dart';
import '../drawable/text_title.dart';
import 'main_page.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppResources.mainColors,
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(
                  top: 50, right: 15, left: 15, bottom: 15),
              child: Row(
                children: [
                  InkWell(
                   onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const MainPage(),
                        ),
                      );
                    },
                    child: const Icon(
                      Icons.chevron_left,
                      color: AppResources.textColorsTitle,
                      size: 35,
                    ),
                  ),
                   AppCircleAvatar(
                    img:
                        'https://thienthumobile.vn/tai-hinh-gai-dep/imager_7_61251_700.jpg',
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextTitle(
                          text: 'Carolyne Agustine',
                          size: 20,
                          color: AppResources.textColorsTitle,
                        ),
                        TextSubTitle(
                          text: '26 mintes ago',
                          color: AppResources.textColorsSubTitle,
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(right: 10),
                    width: 25,
                    height: 25,
                    child: SvgPicture.asset(
                      'assets/icons/dots-horizontal.svg',
                      color: AppResources.textColorsSubTitle,
                    ),
                  ),
                ],
              ),
            ),
            const Expanded(child: CardDetailReviews()),
            const ContainerSend(),
          ],
        ),
      ),
    );
  }
}

class ContainerSend extends StatelessWidget {
  const ContainerSend({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 85,
      padding: const EdgeInsets.only(top: 10, left: 15, right: 15, bottom: 25),
      color: Colors.white,
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 20),
        width: MediaQuery.of(context).size.width * 1,
        decoration: BoxDecoration(
          color: const Color.fromARGB(255, 190, 190, 190).withOpacity(0.1),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Stack(
          children: [
            const TextField(
              style: TextStyle(fontSize: 20),
              decoration: InputDecoration(
                hintText: "Write comment here...",
                hintStyle: TextStyle(
                  fontSize: 20.0,
                  color: AppResources.textColorsHint,
                ),
                border: InputBorder.none,
              ),
            ),
            Positioned(
              right: 5,
              top: 4,
              bottom: 4,
              child: Container(
                height: 40,
                width: 40,
                padding: const EdgeInsets.all(13),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: AppResources.mainIcons1),
                child: SvgPicture.asset(
                  'assets/icons/send.svg',
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CardDetailReviews extends StatelessWidget {
  const CardDetailReviews({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.all(15),
                child: TextTitle(
                  text: 'Secangkir jawa',
                  size: 25,
                ),
              ),
              Wrap(
                children: List.generate(
                  5,
                  (index) => Icon(Icons.star,
                      size: 40,
                      color: index < 5
                          ? AppResources.starColor
                          : AppResources.textColorsTitle1),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(15),
                child: TextSubTitle(
                  text:
                      'I\'m very happy with the services. I think this is the best cafe in Yogyakar,',
                  color: AppResources.textColorsContent,
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 225,
                      height: 125,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: const DecorationImage(
                            image: NetworkImage(
                              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1c-bTW9WWX9LkmEwWq14iEbqjnmupSE7kCA&usqp=CAU',
                            ),
                            fit: BoxFit.cover),
                      ),
                    ),
                    Stack(
                      children: [
                        Container(
                          width: 125,
                          height: 125,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: const DecorationImage(
                                image: NetworkImage(
                                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuLkxTm23gvOkFK--s0NKs1phhDquaJ2l2RQ&usqp=CAU',
                                ),
                                fit: BoxFit.cover),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: 120,
                          height: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.black.withOpacity(0.3),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              TextTitle(
                                text: '6',
                                color: Colors.white,
                                size: 35,
                              ),
                              TextTitle(
                                text: 'more',
                                size: 25,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                    top: 10, bottom: 10, left: 15, right: 15),
                padding: const EdgeInsets.all(15),
                width: double.maxFinite,
                decoration: BoxDecoration(
                  color: AppResources.cardColor.withOpacity(0.4),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextTitle(
                          text: 'Cartegory',
                          size: 18,
                          color: AppResources.textColorsSubTitle1,
                        ),
                        const SizedBox(height: 5),
                        TextSubTitle(text: 'Food & Drink')
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextTitle(
                          text: 'Location',
                          size: 18,
                          color: AppResources.textColorsSubTitle1,
                        ),
                        const SizedBox(height: 5),
                        TextSubTitle(text: 'Seacang Yogyakarta')
                      ],
                    )
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 45, right: 45),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    AppIcons(
                      assetIcon: 'assets/icons/thumbs-up-hand-symbol.svg',
                      text: '68',
                      sizeText: 20,
                    ),
                    AppIcons(
                      assetIcon: 'assets/icons/thumbs-down-silhouette.svg',
                      text: '68',
                      sizeText: 20,
                    ),
                    AppIcons(
                      assetIcon: 'assets/icons/Chat.svg',
                      text: '68',
                      sizeText: 20,
                    ),
                    AppIcons(
                      assetIcon: 'assets/icons/redo.svg',
                      text: '68',
                      sizeText: 20,
                    ),
                  ],
                ),
              ),
              const BoxComment(),
            ],
          ),
        ),
      ),
    );
  }
}

class BoxComment extends StatelessWidget {
  const BoxComment({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: 5,
      itemBuilder: (context, index) => Container(
        alignment: Alignment.topLeft,
        padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
        color: AppResources.boxCommentColor,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppCircleAvatar(
              img:
                  'https://thienthumobile.vn/tai-hinh-gai-dep/imager_7_61251_700.jpg',
            ),
            Flexible(
              child: Container(
                margin: const EdgeInsets.only(top: 5, left: 10, bottom: 10),
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                  color: AppResources.containerBox,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextTitle(
                      text: 'Douglas Schnei',
                      size: 18,
                    ),
                    const SizedBox(height: 10),
                    TextSubTitle(
                      text:
                          'Thanks for the information. Your review help me very much :D',
                      color: AppResources.textColorsTitle1,
                      size: 16,
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        TextSubTitle(
                          text: '3m ago',
                          size: 16,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        TextTitle(
                          text: 'Like',
                          color: AppResources.textColorsSubTitle1,
                          size: 18,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        TextTitle(
                          text: 'Relpy',
                          color: AppResources.textColorsSubTitle1,
                          size: 18,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        const CircleAvatar(
                          radius: 18,
                          backgroundImage: NetworkImage(
                            'https://thienthumobile.vn/tai-hinh-gai-dep/imager_7_61251_700.jpg',
                          ),
                        ),
                        const SizedBox(width: 10),
                        TextTitle(
                          text: 'Eleanor Waters',
                          size: 18,
                        ),
                        const SizedBox(width: 10),
                        const Flexible(
                          child: Text(
                            'I aggreed with you',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
