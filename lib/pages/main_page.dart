import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewapp_hao/pages/navipages/reviews_page.dart';
import 'package:reviewapp_hao/pages/navipages/search_page.dart';
import 'package:reviewapp_hao/values/colors.dart';

import '../drawable/app_buttomnav.dart';
import 'navipages/notifications_page.dart';
import 'navipages/profile_page.dart';
import 'navipages/rate_share_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late int currentIndex = 0;
  List pages = [
    const ReviewsPage(),
    const SearchPage(),
    const NotificationsPage(),
    const ProfilePage()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: [
            pages[currentIndex],
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                height: 100,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          currentIndex = 0;
                        });
                      },
                      child: AppBottomNav(
                        isIcon: currentIndex == 0 ? true : false,
                        assetIcon: 'assets/icons/Reviews.svg',
                        text: 'Reviews',
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          currentIndex = 1;
                        });
                      },
                      child: AppBottomNav(
                        isIcon: currentIndex == 1 ? true : false,
                        assetIcon: 'assets/icons/Search.svg',
                        text: 'Search',
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const RateOrSharePage(),
                          ),
                        );
                      },
                      child: Container(
                        height: 50,
                        width: 50,
                        padding: const EdgeInsets.all(13),
                        margin: const EdgeInsets.only(bottom: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: AppResources.mainIcons1),
                        child: SvgPicture.asset(
                          'assets/icons/edit.svg',
                          color: Colors.white,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          currentIndex = 2;
                        });
                      },
                      child: AppBottomNav(
                        isIcon: currentIndex == 2 ? true : false,
                        assetIcon: 'assets/icons/notification.svg',
                        text: 'Notifications',
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          currentIndex = 3;
                        });
                      },
                      child: AppBottomNav(
                        isIcon: currentIndex == 3 ? true : false,
                        assetIcon: 'assets/icons/user.svg',
                        text: 'Profile',
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
