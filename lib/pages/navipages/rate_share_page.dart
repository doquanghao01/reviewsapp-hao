import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:reviewapp_hao/drawable/text_subtitle.dart';
import 'package:reviewapp_hao/drawable/text_title.dart';
import 'package:reviewapp_hao/values/colors.dart';

import '../../drawable/app_cardbuttom.dart';
import '../main_page.dart';

class RateOrSharePage extends StatefulWidget {
  const RateOrSharePage({Key? key}) : super(key: key);

  @override
  State<RateOrSharePage> createState() => _RateOrSharePageState();
}

class _RateOrSharePageState extends State<RateOrSharePage> {
  final List<String> genderItems = ['Drinks', 'Cake', 'Foods'];

  String? selectedValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppResources.mainColors,
        padding: const EdgeInsets.only(top: 50),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 20, left: 20, right: 20, bottom: 20),
              child: Row(
                children: [
                  Expanded(
                    child: TextTitle(
                      text: 'Rate & Share',
                      color: AppResources.textColorsTitle,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const MainPage(),
                        ),
                      );
                    },
                    child: TextSubTitle(
                      text: 'Cancel',
                      color: AppResources.textColorsSubTitle,
                      size: 25,
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  padding: const EdgeInsets.all(15),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextSubTitle(
                        text: 'Review Tile',
                        size: 25,
                        color: AppResources.textColorsContent,
                      ),
                      const ReviewTittle(),
                      const SizedBox(height: 20),
                      TextSubTitle(
                        text: 'Whats\' Your Rate?',
                        size: 25,
                        color: AppResources.textColorsContent,
                      ),
                      const SizedBox(height: 20),
                      const YourRate(),
                      const SizedBox(
                        height: 20,
                      ),
                      TextSubTitle(
                        text: 'Cayrgory',
                        size: 25,
                        color: AppResources.textColorsContent,
                      ),
                      const SizedBox(height: 30),
                      Cayrgory(),
                      const SizedBox(height: 20),
                      TextSubTitle(
                        text: 'Location',
                        size: 25,
                        color: AppResources.textColorsContent,
                      ),
                      const SizedBox(height: 20),
                      const AppCardButtom(
                        assetIcon: 'assets/icons/maps-and-flags.svg',
                        text:
                            'Choose the location if you want to review it specifically',
                      ),
                      const SizedBox(height: 20),
                      TextSubTitle(
                        text: 'Your Comment',
                        size: 25,
                        color: AppResources.textColorsContent,
                      ),
                      const SizedBox(height: 20),
                      Container(
                        padding: const EdgeInsets.all(20),
                        height: 160,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: AppResources.rimColor.withOpacity(0.2),
                              width: 1.0),
                        ),
                        child: const TextField(
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: null,
                          minLines: 1,
                          decoration: InputDecoration(
                            hintText: "Write your comment here...",
                            hintStyle: TextStyle(
                              fontSize: 20,
                              color: AppResources.textColorsHint,
                            ),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      TextSubTitle(
                        text: 'Upload Photos',
                        size: 25,
                        color: AppResources.textColorsContent,
                      ),
                      const SizedBox(height: 20),
                      const AppCardButtom(
                        sizeHeight: 120,
                        assetIcon: 'assets/icons/image.svg',
                        text: 'Click to add photo',
                      ),
                      const SizedBox(height: 20),
                      TextSubTitle(
                        text: 'Upload Videos',
                        size: 30,
                        color: AppResources.textColorsContent,
                      ),
                      const SizedBox(height: 20),
                      const AppCardButtom(
                        sizeHeight: 120,
                        assetIcon: 'assets/icons/video-camera.svg',
                        text: 'Click to add video',
                      ),
                      const SizedBox(height: 20),
                      Container(
                        width: double.maxFinite,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: AppResources.buttomColor.withOpacity(0.2),
                        ),
                        child: Center(
                          child: TextTitle(
                            text: 'Submit Your Review',
                            size: 25,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  DropdownButtonFormField2<String> Cayrgory() {
    return DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      hint: TextSubTitle(
        text: 'Select Category',
        size: 25,
      ),
      icon: const Icon(
        Icons.keyboard_arrow_down_rounded,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 50,
      buttonPadding: const EdgeInsets.only(left: 30, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: genderItems
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: TextSubTitle(
                  text: item,
                  color: AppResources.textColorsContent,
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'you don\'t have data';
        }
      },
      onChanged: (value) {},
      onSaved: (value) {
        selectedValue = value.toString();
      },
    );
  }
}

class YourRate extends StatelessWidget {
  const YourRate({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: List.generate(
        5,
        (index) => Icon(Icons.star,
            size: 40,
            color: index < 0
                ? AppResources.starColor
                : AppResources.textColorsHint),
      ),
    );
  }
}

class ReviewTittle extends StatelessWidget {
  const ReviewTittle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const TextField(
      style: TextStyle(
        fontSize: 30,
        fontWeight: FontWeight.bold,
      ),
      decoration: InputDecoration(
        hintText: "Enter the Title",
        hintStyle: TextStyle(
          fontSize: 30.0,
          color: AppResources.textColorsHint,
        ),
      ),
    );
  }
}
