import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewapp_hao/drawable/text_subtitle.dart';
import 'package:reviewapp_hao/drawable/text_title.dart';
import 'package:reviewapp_hao/values/colors.dart';

import '../../drawable/app_card_rieviews.dart';

class ReviewsPage extends StatelessWidget {
  const ReviewsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: AppResources.mainColors,
        ),
        Container(
          padding:
              const EdgeInsets.only(top: 50, right: 15, left: 15, bottom: 100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextTitle(
                        text: 'Reviews',
                        color: AppResources.textColorsTitle,
                      ),
                      const SizedBox(height: 5),
                      TextSubTitle(
                        text: 'Browse any reviews for your reference',
                        color: AppResources.textColorsSubTitle,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 25,
                    height: 25,
                    child: SvgPicture.asset(
                      'assets/icons/menu.svg',
                      color: AppResources.textColorsSubTitle,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Expanded(
                child: ListView.builder(
                  itemCount: 10,
                  itemBuilder: (context, index) => CardReviews(
                    index: index,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
