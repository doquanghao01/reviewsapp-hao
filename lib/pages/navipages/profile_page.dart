import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:reviewapp_hao/drawable/app_circle_avatar.dart';
import 'package:reviewapp_hao/drawable/text_subtitle.dart';
import 'package:reviewapp_hao/drawable/text_title.dart';

import '../../drawable/app_card_rieviews.dart';
import '../../values/colors.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.maxFinite,
      height: double.maxFinite,
      child: Stack(
        children: [
          Positioned(
            left: 0,
            right: 0,
            child: Container(
              width: double.maxFinite,
              height: 280,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        'https://recmiennam.com/wp-content/uploads/2018/03/hinh-nen-hoa-lan-dep-1.jpg'),
                    fit: BoxFit.cover),
              ),
              child: Center(
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: 32,
                  child: AppCircleAvatar(
                    radius: 28,
                    img:
                        'https://thienthumobile.vn/tai-hinh-gai-dep/imager_7_61251_700.jpg',
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            right: 20,
            top: 60,
            child: SizedBox(
              width: 24,
              height: 24,
              child: SvgPicture.asset(
                'assets/icons/edit.svg',
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            bottom: 80,
            left: 0,
            right: 0,
            top: 200,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                padding: const EdgeInsets.only(top: 20, right: 20, left: 20),
                decoration: const BoxDecoration(
                  color: AppResources.WhiteSmoke,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Column(
                  children: [
                    TextTitle(text: 'Carolyne Agustine'),
                    Container(
                      padding:
                          const EdgeInsets.only(left: 40, right: 40, top: 20),
                      child: TextSubTitle(
                          text:
                              'Hey there! I\'m using this app for rate & review anything!',
                          isCenter: true),
                    ),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            TextTitle(
                              text: '386',
                              size: 40,
                            ),
                            const SizedBox(height: 10),
                            TextSubTitle(
                              text: 'Followers',
                              size: 25,
                            )
                          ],
                        ),
                        Column(
                          children: [
                            TextTitle(
                              text: '364',
                              size: 40,
                            ),
                            const SizedBox(height: 10),
                            TextSubTitle(
                              text: 'Following',
                              size: 25,
                            )
                          ],
                        )
                      ],
                    ),
                    const SizedBox(height: 30),
                    const RowTitle(
                      amount: '683',
                      title: 'My Reviews',
                    ),
                    const SizedBox(height: 20),
                    CardReviews(
                      index: 1,
                    ),
                    const SizedBox(height: 20),
                    const RowTitle(
                      amount: '382',
                      title: 'My Followers',
                    ),
                    const SizedBox(height: 20),
                    const MyFollow(),
                    const SizedBox(height: 20),
                    const RowTitle(
                      amount: '382',
                      title: 'My Following',
                    ),
                    const SizedBox(height: 20),
                    const MyFollow(),
                    const SizedBox(height: 20),
                    const RowTitle(
                      isamount: false,
                      title: 'Media',
                    ),
                    const SizedBox(height: 20),
                    Container(
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: List.generate(
                          3,
                          (index) => Container(
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: const DecorationImage(
                                  image: NetworkImage(
                                      'https://znews-photo.zingcdn.me/w660/Uploaded/NokaRW/2015_04_24/anh_1_xaqt.jpg'),
                                  fit: BoxFit.cover),
                            ),
                          ),
                        )
                      ),
                    ),
                    const SizedBox(height: 40),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MyFollow extends StatelessWidget {
  const MyFollow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: List.generate(
          4,
          (index) => Column(
            children: [
              AppCircleAvatar(
                img:
                    'https://thienthumobile.vn/tai-hinh-gai-dep/imager_7_61251_700.jpg',
              ),
              const SizedBox(height: 10),
              TextSubTitle(
                text: 'Alexander',
                color: const Color.fromRGBO(83, 95, 117, 1),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class RowTitle extends StatelessWidget {
  const RowTitle({
    Key? key,
    required this.title,
    this.amount = '',
    this.isamount = true,
  }) : super(key: key);
  final String title;
  final String amount;
  final bool isamount;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Row(
            children: [
              TextTitle(
                text: title,
                size: 25,
              ),
              Visibility(
                visible: isamount,
                child: TextSubTitle(
                  text: '($amount)',
                  size: 25,
                ),
              ),
            ],
          ),
        ),
        TextSubTitle(
          text: 'See All',
          size: 22,
          color: const Color.fromRGBO(90, 83, 167, 1),
        ),
      ],
    );
  }
}
