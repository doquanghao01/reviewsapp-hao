import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:reviewapp_hao/drawable/text_subtitle.dart';
import 'package:reviewapp_hao/values/colors.dart';

import '../../drawable/app_circle_avatar.dart';
import '../../drawable/text_title.dart';

class NotificationsPage extends StatelessWidget {
  const NotificationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppResources.mainColors,
      padding: const EdgeInsets.only(top: 60, bottom: 80),
      child: Column(
        children: [
          Container(
            margin:
                const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
            child: Row(
              children: [
                Expanded(
                  child: TextTitle(
                    text: 'Notifications',
                    color: AppResources.textColorsTitle,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  width: 25,
                  height: 25,
                  child: SvgPicture.asset(
                    'assets/icons/dots-horizontal.svg',
                    color: AppResources.textColorsSubTitle,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              width: double.maxFinite,
              height: double.maxFinite,
              decoration:  const BoxDecoration(
                color: AppResources.WhiteSmoke,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  padding: const EdgeInsets.only(top: 20, right: 20, left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextSubTitle(text: 'JUST NOW'),
                      const JustNow(),
                      const SizedBox(height: 15),
                      Container(
                        height: 1,
                        width: double.maxFinite,
                        color: Colors.black.withOpacity(0.2),
                      ),
                      const SizedBox(height: 15),
                      TextSubTitle(text: 'YESTERDAY'),
                      const Yesterday(),
                      const SizedBox(height: 15),
                      Container(
                        height: 1,
                        width: double.maxFinite,
                        color: Colors.black.withOpacity(0.2),
                      ),
                      const SizedBox(height: 15),
                      TextSubTitle(text: 'THIS WEEk'),
                      Container(padding: const EdgeInsets.only(bottom: 20),child: const ThisWeek()),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class JustNow extends StatelessWidget {
  const JustNow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List liststatus = [0, 4, 2, 3, 0, 1];
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: 2,
      itemBuilder: (context, index) => Status(
        index: index,
        status: liststatus[index],
        textName: 'Loretta Fleming',
        textNoti: 'liked your reviw',
        textTime: 'Just now',
      ),
    );
  }
}

class Yesterday extends StatelessWidget {
  const Yesterday({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List liststatus = [2, 1, 4, 3, 0, 1];
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: 3,
      itemBuilder: (context, index) => Status(
        index: index,
        status: liststatus[index],
        textName: 'Elnora Hammond',
        textNoti: 'mentioned you in a commnet',
        textTime: '1 day ago',
      ),
    );
  }
}

class ThisWeek extends StatelessWidget {
  const ThisWeek({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List liststatus = [2, 1, 4, 3, 0, 1];
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: 5,
      itemBuilder: (context, index) => Status(
        index: index,
        status: liststatus[index],
        textName: 'Elnora Hammond',
        textNoti: 'mentioned you in a commnet',
        textTime: 'Mar 30',
      ),
    );
  }
}

class Status extends StatelessWidget {
  int index;
  int status;
  String textName;
  String textNoti;
  String textTime;
  Status(
      {Key? key,
      required this.index,
      required this.status,
      required this.textName,
      required this.textNoti,
      required this.textTime})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Stack(
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: AppCircleAvatar(
                img:
                    'https://thienthumobile.vn/tai-hinh-gai-dep/imager_7_61251_700.jpg',
              ),
            ),
            status != 4
                ? Positioned(
                    right: 0,
                    bottom: 8,
                    child: Container(
                      height: 30,
                      width: 30,
                      padding: const EdgeInsets.all(6),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: SvgPicture.asset(
                        getStatus(status),
                        color: getColor(status),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
        const SizedBox(width: 20),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text.rich(
                TextSpan(
                  children: [
                    WidgetSpan(
                      child: Container(
                        padding: const EdgeInsets.only(right: 5),
                        child: TextTitle(
                          text: textName,
                          size: 23,
                        ),
                      ),
                    ),
                    TextSpan(
                        text: textNoti, style: const TextStyle(fontSize: 16)),
                  ],
                ),
              ),
              TextSubTitle(text: textTime)
            ],
          ),
        ),
      ],
    );
  }

  String getStatus(int status) {
    if (status == 0) {
      return 'assets/icons/thumbs-up-hand-symbol.svg';
    }
    if (status == 1) {
      return 'assets/icons/thumbs-down-silhouette.svg';
    }
    if (status == 2) {
      return 'assets/icons/Chat.svg';
    }
    if (status == 3) {
      return 'assets/icons/redo.svg';
    }
    return '';
  }

  Color getColor(int status) {
    if (status == 0) {
      return Colors.blue;
    }
    if (status == 1) {
      return Colors.black45;
    }
    if (status == 2) {
      return const Color.fromARGB(255, 132, 0, 255);
    }
    if (status == 3) {
      return Colors.green;
    }
    return Colors.white;
  }
}
