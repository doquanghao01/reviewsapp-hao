import 'package:flutter/material.dart';

import 'app_icons.dart';

class AppBottomNav extends StatelessWidget {
  const AppBottomNav({
    Key? key,
    required this.text,
    required this.assetIcon,
    this.isIcon = false,
    this.isText = true,
  }) : super(key: key);
  final String assetIcon;
  final String text;
  final bool isIcon;
  final bool isText;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: 75,
      child: AppIcons(
          isIcon: isIcon,
          isText: isText,
          sizeText: 16,
          assetIcon: assetIcon,
          text: text),
    );
  }
}
