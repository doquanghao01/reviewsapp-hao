import 'package:flutter/material.dart';

class AppCircleAvatar extends StatelessWidget {
   String img;
  final double radius;
   AppCircleAvatar({
    Key? key,
    required this.img,
    this.radius = 25,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: radius,
      backgroundImage: NetworkImage(img),
    );
  }
}
