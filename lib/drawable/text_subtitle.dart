import 'package:flutter/cupertino.dart';
import 'package:reviewapp_hao/values/colors.dart';

class TextSubTitle extends StatelessWidget {
  String text;
  final double size;
  final Color color;
  final bool isCenter;
  TextSubTitle({
    Key? key,
    required this.text,
    this.size = 20,
    this.color = AppResources.textColorsContent1,
    this.isCenter = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
      textAlign: isCenter == true ? TextAlign.center : TextAlign.start,
      style: TextStyle(
        color: color,
        fontSize: size,
      ),
    );
  }
}
