import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import '../values/colors.dart';
import 'app_icons.dart';

class AppCardButtom extends StatelessWidget {
  final String assetIcon;
  final String text;
  final double sizeHeight;
  const AppCardButtom({
    Key? key,
    required this.assetIcon,
    required this.text,
    this.sizeHeight = 150,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      borderType: BorderType.RRect,
      color: AppResources.rimColor.withOpacity(0.2),
      radius: const Radius.circular(12),
      dashPattern: const [8, 4],
      strokeWidth: 2,
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        child: Container(
          height: sizeHeight,
          width: double.maxFinite,
          color: AppResources.boxCommentColor.withOpacity(0.3),
          child: Container(
            padding:
                const EdgeInsets.only(top: 20, bottom: 20, left: 50, right: 50),
            child: AppIcons(
              assetIcon: assetIcon,
              text: text,
              sizeIcon: 50,
              sizeText: 20,
            ),
          ),
        ),
      ),
    );
  }
}
