import 'package:flutter/cupertino.dart';
import 'package:reviewapp_hao/values/colors.dart';

class TextTitle extends StatelessWidget {
  String text;
  final double size;
  final Color color;
  TextTitle({
    Key? key,
    required this.text,
    this.size = 30,
    this.color = AppResources.textColorsTitle1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: size,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
