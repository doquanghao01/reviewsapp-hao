import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:reviewapp_hao/drawable/text_subtitle.dart';
import 'package:reviewapp_hao/drawable/text_title.dart';

import '../pages/detail_page.dart';
import '../values/colors.dart';
import 'app_circle_avatar.dart';
import 'app_icons.dart';

class CardReviews extends StatelessWidget {
  CardReviews({
    Key? key,
     this.index=2,
  }) : super(key: key);
  int index;

  @override
  Widget build(BuildContext context) {
    String text =
        'I\'m very happy with the services. I think this is the best cafe in Yogyakar';
    return Container(
      padding: const EdgeInsets.all(20),
      margin: const EdgeInsets.only(bottom: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: AppResources.containerBox,
      ),
      child: Column(
        children: [
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const DetailPage(),
                ),
              );
            },
            child: Row(
              children: [
                AppCircleAvatar(
                  img:
                      'https://thienthumobile.vn/tai-hinh-gai-dep/imager_7_61251_700.jpg',
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextTitle(
                        text: 'Carolyne Agustine',
                        size: 20,
                      ),
                      TextSubTitle(text: '26 mintes ago')
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  width: 25,
                  height: 25,
                  child: SvgPicture.asset(
                    'assets/icons/dots-horizontal.svg',
                    color: AppResources.textColorsContent1,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          Container(
            width: double.maxFinite,
            height: 1,
            color: AppResources.textColorsContent1,
          ),
          Container(
            margin: const EdgeInsets.only(top: 15, bottom: 15),
            child: TextTitle(
              text: 'Secangkir jawa',
              size: 25,
            ),
          ),
          Wrap(
            children: List.generate(
              5,
              (index) => Icon(Icons.star,
                  size: 40,
                  color: index < 5
                      ? AppResources.starColor
                      : AppResources.textColorsTitle1),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 15, bottom: 15),
            child: Text.rich(
              TextSpan(
                children: [
                  TextSpan(
                    text:
                        text.length > 74 ? "${text.substring(0, 74)}..." : text,
                    style: const TextStyle(
                      fontSize: 20,
                      color: AppResources.textColorsTitle1,
                    ),
                  ),
                  const TextSpan(
                    text: 'See More',
                    style: TextStyle(
                      fontSize: 22,
                      color: AppResources.textColorsContent1,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
          index != 2
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 190,
                      height: 120,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: const DecorationImage(
                            image: NetworkImage(
                              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1c-bTW9WWX9LkmEwWq14iEbqjnmupSE7kCA&usqp=CAU',
                            ),
                            fit: BoxFit.cover),
                      ),
                    ),
                    Stack(
                      children: [
                        Container(
                          width: 120,
                          height: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: const DecorationImage(
                                image: NetworkImage(
                                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuLkxTm23gvOkFK--s0NKs1phhDquaJ2l2RQ&usqp=CAU',
                                ),
                                fit: BoxFit.cover),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: 120,
                          height: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.black.withOpacity(0.3),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              TextTitle(
                                text: '6',
                                color: Colors.white,
                                size: 35,
                              ),
                              TextTitle(
                                text: 'more',
                                size: 25,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              : Container(),
          Container(
            margin: const EdgeInsets.only(top: 10, bottom: 10),
            padding: const EdgeInsets.all(15),
            width: double.maxFinite,
            decoration: BoxDecoration(
              color: AppResources.cardColor.withOpacity(0.4),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextTitle(
                      text: 'Cartegory',
                      size: 18,
                      color: AppResources.textColorsSubTitle1,
                    ),
                    const SizedBox(height: 5),
                    TextSubTitle(text: 'Food & Drink')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextTitle(
                      text: 'Location',
                      size: 18,
                      color: AppResources.textColorsSubTitle1,
                    ),
                    const SizedBox(height: 5),
                    TextSubTitle(text: 'Seacang Yogyakarta')
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                AppIcons(
                  assetIcon: 'assets/icons/thumbs-up-hand-symbol.svg',
                  text: '68',
                  sizeText: 20,
                ),
                AppIcons(
                  assetIcon: 'assets/icons/thumbs-down-silhouette.svg',
                  text: '68',
                  sizeText: 20,
                ),
                AppIcons(
                  assetIcon: 'assets/icons/Chat.svg',
                  text: '68',
                  sizeText: 20,
                ),
                AppIcons(
                  assetIcon: 'assets/icons/redo.svg',
                  text: '68',
                  sizeText: 20,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
