import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:reviewapp_hao/drawable/text_title.dart';

import '../values/colors.dart';
import 'text_subtitle.dart';

class AppIcons extends StatelessWidget {
  final String assetIcon;
  final String text;
  final bool isIcon;
  final bool isText;
  final double sizeIcon;
  final double sizeText;
  const AppIcons(
      {Key? key,
      required this.assetIcon,
      this.text = '',
      this.isIcon = false,
      this.isText = true,
      this.sizeIcon = 24,
      this.sizeText = 15})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: sizeIcon,
          height: sizeIcon,
          child: SvgPicture.asset(
            assetIcon,
            color: isIcon == true
                ? AppResources.mainIcons1
                : AppResources.mainIcons,
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        isText == true
            ? TextSubTitle(
                text: text,
                size: sizeText,
                isCenter: true,
                color: isIcon == true
                    ? AppResources.mainIcons1
                    : AppResources.mainIcons)
            : Container()
      ],
    );
  }
}
